using System.IO;
using RealFizzBuzz.Core;

namespace RealFizzBuzz
{
    public static class FizzBuzzWriter
    {
        public static void Write(TextWriter output, int minimumValue, int maximumValue)
        {
            var statistics = new Statistics();

            for (var value = minimumValue; value <= maximumValue; ++value)
            {
                var category = Categorizer.GetCategory(value);

                statistics.Add(category);

                output.Write(category == Category.integer ? value.ToString() : category.ToString());

                if (value != maximumValue)
                    output.Write(' ');
            }

            output.WriteLine();

            foreach (var kv in statistics.Enumerate())
            {
                output.Write(kv.Key);
                output.Write(": ");
                output.WriteLine(kv.Value);
            }
        }
    }
}