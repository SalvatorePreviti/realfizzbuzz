﻿using System;

namespace RealFizzBuzz
{
    class Program
    {
        static int Main(string[] args)
        {
            ProgramArguments programArguments;
            try
            {
                programArguments = new ProgramArguments(args);
            }
            catch (ArgumentException ex)
            {
                Console.Error.WriteLine($"Error: {ex.Message}");
                Console.Error.WriteLine("Required arguments: <minimum value> <maximum value>");
                return -1;
            }

            FizzBuzzWriter.Write(
                Console.Out,
                programArguments.MinimumValue,
                programArguments.MaximumValue);

            return 0;
        }
    }
}
