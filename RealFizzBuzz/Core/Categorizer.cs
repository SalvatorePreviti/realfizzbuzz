using System;

namespace RealFizzBuzz.Core
{
    public static class Categorizer
    {
        public static Category GetCategory(int value)
        {
            if (value == 0)
                return Category.integer;

            if (value == int.MinValue)
                return Category.lucky;

            for (var d = Math.Abs(value); d >= 3; d /= 10)
                if (d % 10 == 3)
                    return Category.lucky;

            if (value % 15 == 0)
                return Category.fizzbuzz;

            if (value % 5 == 0)
                return Category.buzz;

            if (value % 3 == 0)
                return Category.fizz;

            return Category.integer;
        }
    }
}