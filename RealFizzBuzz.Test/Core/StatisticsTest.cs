using System.Linq;
using RealFizzBuzz.Core;
using Xunit;

namespace RealFizzBuzz.Test.Core
{
    public class StatisticsTest
    {
        [Fact]
        public void StatisticsStoresCorrectValues()
        {
            var statistics = new Statistics();

            statistics.Add(Category.buzz);
            statistics.Add(Category.fizz);
            statistics.Add(Category.lucky);
            statistics.Add(Category.lucky);
            statistics.Add(Category.fizzbuzz);
            statistics.Add(Category.buzz);
            statistics.Add(Category.integer);
            statistics.Add(Category.fizz);
            statistics.Add(Category.lucky);

            var lookup = statistics
                .Enumerate()
                .ToDictionary(kv => kv.Key, kv => kv.Value);

            Assert.Equal(3, lookup[Category.lucky]);
            Assert.Equal(2, lookup[Category.fizz]);
            Assert.Equal(2, lookup[Category.buzz]);
            Assert.Equal(1, lookup[Category.fizzbuzz]);
            Assert.Equal(1, lookup[Category.integer]);
        }

        [Fact]
        public void ConstructorBuildsCategoriesInRightOrder()
        {
            var expected = new[] {
                Category.fizz,
                Category.buzz,
                Category.fizzbuzz,
                Category.lucky,
                Category.integer
            };

            Assert.True(
                Enumerable.SequenceEqual(
                    expected,
                    from kv in new Statistics().Enumerate() select kv.Key),
                $"Categories list is wrong, check the definition of enum {nameof(Category)}");
        }
    }
}