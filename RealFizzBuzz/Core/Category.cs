namespace RealFizzBuzz.Core
{
    public enum Category
    {
        fizz,
        buzz,
        fizzbuzz,
        lucky,
        integer
    }
}
