using System;

namespace RealFizzBuzz
{
    public class ProgramArguments
    {
        private readonly int _minimumValue;
        private readonly int _maximumValue;

        public ProgramArguments(params string[] args)
        {
            if (args == null)
                throw new ArgumentNullException(nameof(args));

            if (args.Length != 2)
                throw new ArgumentException($"Expected 2 arguments, received {args.Length}");

            if (!int.TryParse(args[0], out _minimumValue))
                throw new ArgumentException("Minimum value is not a valid integer number");

            if (!int.TryParse(args[1], out _maximumValue))
                throw new ArgumentException("Maximum value is not a valid integer number");

            if (_minimumValue > _maximumValue)
                throw new ArgumentException("Minimum value cannot be greater than maximum value");
        }

        public int MinimumValue => _minimumValue;

        public int MaximumValue => _maximumValue;
    }
}