using System;
using System.IO;
using Xunit;

namespace RealFizzBuzz.Test
{
    public class FizzBuzzWriterTest
    {
        [Fact]
        public void SimpleTestCase()
        {
            var expected =
                "1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz" + Environment.NewLine +
                "fizz: 4" + Environment.NewLine +
                "buzz: 3" + Environment.NewLine +
                "fizzbuzz: 1" + Environment.NewLine +
                "lucky: 2" + Environment.NewLine +
                "integer: 10" + Environment.NewLine;

            var writer = new StringWriter();
            FizzBuzzWriter.Write(writer, 1, 20);
            Assert.Equal(expected, writer.ToString());
        }
    }
}