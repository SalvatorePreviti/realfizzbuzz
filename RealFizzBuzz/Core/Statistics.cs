using System;
using System.Collections.Generic;

namespace RealFizzBuzz.Core
{
    public class Statistics
    {
        private readonly SortedDictionary<Category, int> _table;

        public Statistics()
        {
            _table = new SortedDictionary<Category, int>();
            foreach (Category category in Enum.GetValues(typeof(Category)))
                _table[category] = 0;
        }

        public void Add(Category category)
        {
            ++_table[category];
        }

        public IEnumerable<KeyValuePair<Category, int>> Enumerate()
        {
            return _table;
        }
    }
}
