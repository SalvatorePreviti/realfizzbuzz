# RealFizzBuzz 0.3

Implementation in DotNetCore 1.1.0 with XUnit tests.

# How to compile and run

Prerequisites:

- You need [DotNetCore 1.1.0 SDK](https://www.microsoft.com/net/download/core) in your machine.
- Open a console in the Solution folder
- Run the command `dotnet restore`

To compile and run the application

- Go to the subdirectory `cd RealFizzBuzz`
- Run the command `dotnet run 1 20`

To compile and run tests

- Go to the subdirectory `cd RealFizzBuzz.Test`
- Run the command `dotnet test`

# Project details

The application was developed on Mac OS X with Visual Studio Code and DotNetCore CLI.
Tested also on Windows OS.

A "test first" approach was used.
The code is by purpose extremely simple and self explanatory, with very limited abstraction and limited application of SOLID principles, to keep it simple and clean.

The main fizz-buzz logic is in the static method `RealFizzBuzz.Categorizer.GetCategory`, it processes an integer value and return the fizz-buzz category.
The static method `RealFizzBuzz.FizzBuzzWriter.Write` is able to write the fizz-buzz sequence to a stream.
The class `RealFizzBuzz.Core.Statistics` is able to count the occourrences of a given category. It is implemented using reflection over  `RealFizzBuzz.Core.Category` enum, and will list categories in the order they appears in the enum.
The class `RealFizzBuzz.ProgramArguments` is responsible to parse arguments passed to the program.
The method `RealFizzBuzz.Program.Main` puts all things together.

# Author

Salvatore Previti