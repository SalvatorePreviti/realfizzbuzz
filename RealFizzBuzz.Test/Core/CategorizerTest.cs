using RealFizzBuzz.Core;
using Xunit;

namespace RealFizzBuzz.Test.Core
{
    public class CategorizerTest
    {
        [Fact]
        public void FizzForMultipleOf3()
        {
            foreach (var value in new[] { 6, -6, 9, -9, 12, -12, 21, 24 })
                Assert.Equal(Category.fizz, Categorizer.GetCategory(value));
        }

        [Fact]
        public void BuzzForMultipleOf5()
        {
            foreach (var value in new[] { 5, -5, 25, -25, 50, -50, 5000 })
                Assert.Equal(Category.buzz, Categorizer.GetCategory(value));
        }

        [Fact]
        public void FizzBuzzForMultipleOf15()
        {
            foreach (var value in new[] { 15, -15, 150, -150 })
                Assert.Equal(Category.fizzbuzz, Categorizer.GetCategory(value));
        }

        [Fact]
        public void IntegerForOtherCases()
        {
            foreach (var value in new[] { -2, -1, 0, 1, 2, 17, 22 })
                Assert.Equal(Category.integer, Categorizer.GetCategory(value));
        }

        [Fact]
        public void LuckyIfContainsDigit3()
        {
            foreach (var value in new[] { 3, -3, 30, -13, 13, 30, 31, 3334, 5432, int.MinValue, int.MaxValue })
                Assert.Equal(Category.lucky, Categorizer.GetCategory(value));
        }

    }
}