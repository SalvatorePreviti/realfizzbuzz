using System;
using Xunit;

namespace RealFizzBuzz.Test
{
    public class ProgramArgumentsTest
    {
        [Fact]
        public void ConstructorThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => new ProgramArguments(null as string[]));
        }

        [Fact]
        public void ConstructorThrowsArgumentExceptionIfArgumentsAreNot2()
        {
            Assert.Throws<ArgumentException>(() => new ProgramArguments());
            Assert.Throws<ArgumentException>(() => new ProgramArguments("1"));
            Assert.Throws<ArgumentException>(() => new ProgramArguments("1", "2", "3"));
        }

        [Theory]
        [InlineData(null, "20")]
        [InlineData("5", null)]
        [InlineData("xxx", "20")]
        [InlineData("10", "xxx")]
        [InlineData("aaa", "xxx")]
        public void ConstructorThrowsArgumentExceptionIfParseFailed(string min, string max)
        {
            Assert.Throws<ArgumentException>(() => new ProgramArguments(min, max));
        }

        [Theory]
        [InlineData(30, 20)]
        [InlineData(30, -1)]
        [InlineData(-30, -40)]
        [InlineData(int.MaxValue, int.MinValue)]
        public void ConstructorThrowsArgumentExceptionIfMinGreaterThanMax(string min, string max)
        {
            Assert.Throws<ArgumentException>(() => new ProgramArguments(min.ToString(), max.ToString()));
        }

        [Theory]
        [InlineData(1, 1)]
        [InlineData(1, 2)]
        [InlineData(-12, 20)]
        [InlineData(-1, 2)]
        [InlineData(0, 20)]
        [InlineData(int.MinValue, int.MaxValue)]
        public void ConstructorSetsProperties(int min, int max)
        {
            var a = new ProgramArguments(min.ToString(), max.ToString());
            Assert.Equal(min, a.MinimumValue);
            Assert.Equal(max, a.MaximumValue);
        }
    }
}